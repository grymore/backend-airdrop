const { google } = require('googleapis');
const jwt = require('jsonwebtoken');
const https = require('https');
const oauth2 = google.oauth2('v2');

const nanoid = require('nanoid').nanoid;

// Including our config file
const CONFIG = require("./config");

const oauth2Client = new google.auth.OAuth2(
  CONFIG.oauth2Credentials.client_id,
  CONFIG.oauth2Credentials.client_secret,
  CONFIG.oauth2Credentials.redirect_uris[0]
);

const getConnectionUrl = () => {
  const url = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent',
    scope: CONFIG.oauth2Credentials.scopes.join(' ')
  });
  
  //console.log('@url login ? ', url);
  return url;
}

const service = google.youtube({
  version: 'v3',
  auth: CONFIG.oauth2Credentials
});


module.exports = {
  async oauthURL(ctx) {
    const url = getConnectionUrl();
    console.log('@Login URL ? ', url);
    return { url };
  },
  async oauthCallback(ctx) {
    // oauth2Client init connection
    // const oauth2Client = new google.auth.OAuth2
    
    // console.log(ctx.request.query.code);
    
    if (ctx.request.query.error) {
      console.log('@1', ctx.request.query.error);
      return ctx.response.redirect("https://app.tokendooit.com/?x#/oa2gc/0/0/0/0");
    } else {
      var codeToken = ctx.request.query.code;
      // console.log('@1.5 -- codeToken? ', codeToken);
      //const jwtxxx = await oauth2Client.getToken(codeToken, async (err, token) => {
      //  if (err) {
      //    console.log('@2', err);
      //    //return ctx.response.redirect("/");
      //    reject(err);
      //  }
      //  jwtToParam = jwt.sign(token, CONFIG.JWTsecret);
      //  console.log("@3", jwtToParam);
      //  resolve({ ok: true, jwt: jwtToParam });
      //});

      const { tokens } = await oauth2Client.getToken(codeToken);
       
      // console.log('@JWT Token ? ', tokens);
      
      oauth2Client.setCredentials(tokens);
      
      const usr_info = await oauth2.userinfo.get({auth: oauth2Client});
      // console.log('@google data ? ', usr_info.data);
         
      //const yt = await service.subscriptions.list({
      //  auth: oauth2Client,
      //  'part' : [
      //    'contentDetails',
      //    'id',
      //    'snippet',
      //    'subscriberSnippet'
      //  ],
      //  // "mySubscribers": true,
      //  "mine": true,
      //  //"channelId": 'UCp3k2-LKYBgb4DJ986RpYwA',
      //  "forChannelId": 'UCp3k2-LKYBgb4DJ986RpYwA',
      //  "order": 'alphabetical',
      //  "prettyPrint": true,
      //  "maxResults": 50
      //});
      //console.log('@youtube data ? ', (yt.data.pageInfo.totalResults > 0) ? yt.data.items[0].snippet.thumbnails : yt.data);
      
      //const nanoid = require('nanoid');
      var fakeYTid = nanoid();

      const results = {
        google: usr_info.data,
        //youtube: (yt.data.pageInfo.totalResults > 0) ? yt.data.items[0] : null
      };
      console.log('Google | Fake Youtube results ? ', results, fakeYTid);

      var urlParams = [
        results.google.email ? 1 : 0,
        encodeURIComponent(results.google.email),
        //results.youtube ? encodeURIComponent(results.youtube.snippet.channelId) : 0,
        fakeYTid,
        //results.youtube ? encodeURIComponent(results.youtube.snippet.thumbnails.default.url) : 0
        0
      ];
      console.log('@URL Params ? ', urlParams);

      // todo:
      // SAVE TO STRAPI DB
      if(results.youtube){
        const data = JSON.stringify({
          pk: results.google.email,
          //id_channel: encodeURIComponent(results.youtube.snippet.channelId)
          id_channel: fakeYTid
        })

const options = {
  hostname: 'admx.tokendooit.com',
  port: 443,
  path: '/youtubes',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  }
}

const req = https.request(options, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', d => {
    process.stdout.write(d)
  })
})

req.on('error', error => {
  console.error(error)
})

req.write(data)
req.end()

      }
      return ctx.response.redirect(`https://app.tokendooit.com/?x#/oa2gc/${urlParams.join('/')}`);
    }
  },

  async youtubeCheckSubscription(ctx) {
    return { none: true };
  },

  async googleCallback(ctx) {
    //console.log('@googleCallback ? ', ctx.request);
    return { none: true };
  }
};
