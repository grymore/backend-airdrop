module.exports = {
  apps: [
    {
      name: 'strapi-admin',
      script: 'npm',
      args: 'run develop',
    },
  ],
};
