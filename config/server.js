module.exports = ({ env }) => ({
  // host: env('HOST', '167.71.192.137'),
  host: env('HOST', '127.0.0.1'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '4dd9f5628a866cfbff30d118f0bbeb9a'),
    },
  },
});
